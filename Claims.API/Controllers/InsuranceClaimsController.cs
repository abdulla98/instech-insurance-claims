using FluentValidation;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Services.Audit;
using Services.Claim;
using Services.Cover;
using Swashbuckle.AspNetCore.Annotations;

namespace Claims.API.Controllers;

/// <summary>
///This controller handles all operations related to Claims
/// </summary>
[PublicAPI]
[ApiController]
[Route("api/claims")]
public class InsuranceClaimsController : ControllerBase
{
    private readonly IClaimService _claimService;
    private readonly IAuditService _auditService;
    private readonly IValidator<InsuranceClaimRequest> _claimValidator;
    private readonly ICoverService _coverService;

    // Constructor for dependency injection
    /// <summary>
    /// Initializes a new instance of the <see cref="InsuranceClaimsController"/> class.
    /// </summary>
    /// <param name="claimService">The claim service.</param>
    /// <param name="auditService">The audit service.</param>
    /// <param name="claimValidator">The claim validator.</param>
    /// <param name="coverService">The cover service.</param>
    public InsuranceClaimsController(IClaimService claimService, IAuditService auditService,
        IValidator<InsuranceClaimRequest> claimValidator, ICoverService coverService)
    {
        _claimService = claimService;
        _auditService = auditService;
        _claimValidator = claimValidator;
        _coverService = coverService;
    }

    // Retrieves all claims
    /// <summary>
    /// Retrieves all claims asynchronously.
    /// </summary>
    /// <returns>An <see cref="IEnumerable{Claim}"/> representing the list of claims.</returns>
    [SwaggerResponse(200, "Returns list of Claims")]
    [HttpGet]
    public Task<IEnumerable<Claim>> GetAllClaimsAsync()
    {
        return _claimService.GetClaimsAsync();
    }

    // Creates a new claim
    /// <summary>
    /// Creates a new claim.
    /// </summary>
    /// <param name="claimRequest">The claim request data.</param>
    /// <returns>An <see cref="ActionResult"/> representing the result of the operation.</returns>
    [SwaggerResponse(200, "Returns the Created Claim")]
    [HttpPost]
    public async Task<ActionResult> CreateClaimAsync(InsuranceClaimRequest claimRequest)
    {
        var cover = await _coverService.GetCoverByIdAsync(claimRequest.CoverId);
        var claim = new Claim(cover!, claimRequest.Created, claimRequest.Name, claimRequest.Type,
            claimRequest.DamageCost);
        await _claimService.AddItemAsync(claim);
        await _auditService.AuditClaim(claim.Id, "POST");
        return Ok(claim);
    }

    // Deletes a claim by its ID
    /// <summary>
    /// Removes a claim by its ID.
    /// </summary>
    /// <param name="id">The ID of the claim to remove.</param>
    /// <returns>An <see cref="IActionResult"/> representing the result of the operation.</returns>
    [SwaggerResponse(202, "Claim deleted")]
    [HttpDelete("{id}")]
    public async Task<IActionResult> RemoveClaimAsync(string id)
    {
        var existingClaim = await _claimService.GetClaimAsync(id);
        if (existingClaim == null)
        {
            return NotFound($"Claim with ID {id} not found.");
        }

        await _claimService.DeleteItemAsync(id);
        await _auditService.AuditClaim(id, "DELETE");
        return NoContent();
    }

    // Retrieves a claim by its ID
    /// <summary>
    /// Retrieves a claim by its ID.
    /// </summary>
    /// <param name="id">The ID of the claim to retrieve.</param>
    /// <returns>An <see cref="ActionResult"/> representing the result of the operation.</returns>
    [HttpGet("{id}")]
    public async Task<ActionResult<Claim>> GetClaimByIdAsync(string id)
    {
        var claim = await _claimService.GetClaimAsync(id);
        if (claim == null)
        {
            return NotFound($"Claim with ID {id} not found.");
        }
        return Ok(claim);
    }
}