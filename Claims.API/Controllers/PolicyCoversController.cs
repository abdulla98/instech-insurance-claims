using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Services.Audit;
using Services.Cover;
using Swashbuckle.AspNetCore.Annotations;

namespace Claims.API.Controllers;

/// <summary>
///This controller handles all operations related to Covers
/// </summary>
[PublicAPI]
[ApiController]
[Route("api/covers")]
public class PolicyCoversController : ControllerBase
{
    private readonly ILogger<PolicyCoversController> _logger;
    private readonly IAuditService _auditService;
    private readonly ICoverService _coverService;

    // Constructor for dependency injection
    /// <summary>
    /// Initializes a new instance of the <see cref="PolicyCoversController"/> class.
    /// </summary>
    /// <param name="coverService">The cover service.</param>
    /// <param name="auditService">The audit service.</param>
    /// <param name="logger">The logger.</param>
    public PolicyCoversController(ICoverService coverService, IAuditService auditService, ILogger<PolicyCoversController> logger)
    {
        _coverService = coverService;
        _logger = logger;
        _auditService = auditService;
    }

    // Computes the premium for a cover based on the provided parameters
    /// <summary>
    /// Calculates the premium for a cover based on the provided parameters.
    /// </summary>
    /// <param name="coverRequest">The cover request data.</param>
    /// <returns>An IActionResult representing the result of the operation.</returns>
    [SwaggerResponse(200, "Result of the premium computation", typeof(decimal))]
    [SwaggerResponse(400, "Bad request")]
    [HttpGet("compute-premium")]
    public IActionResult CalculatePremium([FromQuery] PolicyCoverRequest coverRequest)
    {
        var premium = _coverService.ComputePremium(coverRequest.StartDate, coverRequest.EndDate, coverRequest.CoverType);
        return Ok(premium);
    }

    // Retrieves all covers
    /// <summary>
    /// Retrieves all covers asynchronously.
    /// </summary>
    /// <returns>An IActionResult representing the result of the operation.</returns>
    [SwaggerResponse(200, "List of all Covers", typeof(IEnumerable<Cover>))]
    [HttpGet]
    public async Task<ActionResult<IEnumerable<Cover>>> GetAllCoversAsync()
    {
        var results = await _coverService.GetAllCoversAsync();
        return Ok(results);
    }

    // Retrieves a cover by its ID
    /// <summary>
    /// Retrieves a cover by its ID.
    /// </summary>
    /// <param name="id">The ID of the cover to retrieve.</param>
    /// <returns>An IActionResult representing the result of the operation.</returns>
    [SwaggerResponse(404, "Cover with that id is not found")]
    [SwaggerResponse(200, "Cover with the passed Id", typeof(Cover))]
    [HttpGet("{id}")]
    public async Task<ActionResult<Cover>> GetCoverByIdAsync(string id)
    {
        var result = await _coverService.GetCoverByIdAsync(id);
        if (result is null) {
            return NotFound();
        }

        return Ok(result);
    }

    // Creates a new cover
    /// <summary>
    /// Creates a new cover.
    /// </summary>
    /// <param name="createCoverRequest">The cover request data.</param>
    /// <returns>An IActionResult representing the result of the operation.</returns>
    [SwaggerResponse(200, "Returns the created Cover", typeof(Cover))] 
    [SwaggerResponse(400, "Bad request")]
    [HttpPost]
    public async Task<ActionResult> CreateCoverAsync([FromBody]PolicyCoverRequest createCoverRequest)
    {
        var cover = new Cover(createCoverRequest.StartDate, createCoverRequest.EndDate, createCoverRequest.CoverType);
        await _coverService.CreateCoverAsync(cover);

        await _auditService.AuditCover(cover.Id, "POST");
        return Ok(cover);
    }
    /// <summary>
    /// Deletes a cover by its ID.
    /// </summary>
    /// <param name="id">The ID of the cover to delete.</param>
    /// <returns>An IActionResult representing the result of the operation.</returns>
    [SwaggerResponse(202, "Cover Deleted")]
    [HttpDelete("{id}")]
    public async Task<IActionResult> RemoveCoverAsync(string id)
    {
        await _auditService.AuditCover(id, "DELETE");
        await _coverService.DeleteCoverAsync(id);
        return NoContent();
    }
}