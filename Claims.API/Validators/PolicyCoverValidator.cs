using FluentValidation;
using Services.Cover;

namespace Claims.API.Validators;

internal class PolicyCoverValidator: AbstractValidator<PolicyCoverRequest>
{
	public PolicyCoverValidator()
	{
		RuleFor(c => c.StartDate)
			.Must(Cover.DateIsNotInPast)
			.WithMessage("StartDate cannot be in the past.");

		RuleFor(c => c.EndDate)
			.Must(Cover.DateIsNotInPast)
			.WithMessage("EndDate cannot be in the past.");

		RuleFor(c => c)
			.Must(c =>  Cover.PeriodIsUnderYear(c.StartDate, c.EndDate))
			.WithMessage("Total insurance period cannot exceed 1 year");
	}
}