using JetBrains.Annotations;
using Services.Cover;

namespace Claims.API.DTos;

/// <summary>
/// Request model for creating new Cover
/// </summary>
[PublicAPI]
public class PolicyCoverRequest

{
	/// <summary>
	/// Start date of the insurance period. Example: 2023-01-01
	/// </summary>
	public DateOnly StartDate { get; set; }
	/// <summary>
	/// End date for the insurance period. Example: 2024-01-01
	/// </summary>
	public DateOnly EndDate { get; set; }
	/// <summary>
	/// Cover Type
	/// </summary>
	public CoverType CoverType { get; set; }
}