namespace Services.Cover;

public abstract class PremiumCalculator
{
    private readonly decimal _baseDailyRate;
    protected abstract decimal CoverTypeMultiplier { get; }
    protected abstract decimal EarlyPeriodDiscountMultiplier { get; }
    protected abstract decimal ExtendedPeriodDiscountMultiplier { get; }
    private decimal AdjustedDailyRate => _baseDailyRate * CoverTypeMultiplier;

    protected PremiumCalculator(decimal baseDailyRate)
    {
        _baseDailyRate = baseDailyRate;
    }
    
    public virtual decimal CalculateTotalPremium(DateOnly coverStartDate, DateOnly coverEndDate)
    {
        var initialPeriodDays = CalculateInitialPeriodDays(coverStartDate, coverEndDate);
        var earlyPeriodDays = CalculateEarlyPeriodDays(coverStartDate, coverEndDate);
        var extendedPeriodDays = CalculateExtendedPeriodDays(coverStartDate, coverEndDate);

        decimal initialPeriodCost = initialPeriodDays * AdjustedDailyRate;
        decimal earlyPeriodCost = earlyPeriodDays * (AdjustedDailyRate * (1 - EarlyPeriodDiscountMultiplier));
        decimal extendedPeriodCost = extendedPeriodDays * (AdjustedDailyRate * (1 - ExtendedPeriodDiscountMultiplier));

        return initialPeriodCost + earlyPeriodCost + extendedPeriodCost;
    }
    
    private static int CalculateExtendedPeriodDays(DateOnly coverStartDate, DateOnly coverEndDate)
    {
        int totalCoverDays = coverEndDate.DayNumber - coverStartDate.DayNumber;
        return totalCoverDays > 180 ? totalCoverDays - 180 : 0;
    }

    private static int CalculateEarlyPeriodDays(DateOnly coverStartDate, DateOnly coverEndDate)
    {
        int totalCoverDays = coverEndDate.DayNumber - coverStartDate.DayNumber;

        if (totalCoverDays <= 30) return 0;
        else if (totalCoverDays > 180) return 150;
        else return totalCoverDays - 30;
    }

    private static int CalculateInitialPeriodDays(DateOnly coverStartDate, DateOnly coverEndDate)
    {
        int totalCoverDays = coverEndDate.DayNumber - coverStartDate.DayNumber;
        return totalCoverDays > 30 ? 30 : totalCoverDays;
    }
}