﻿namespace Core.Models.Audit
{
    public class ClaimAudit: AuditBase
    {
        public string? ClaimId { get; set; }
    }
}
