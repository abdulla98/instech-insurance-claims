namespace Core.Interfaces;

public interface IAuditRepository
{
	Task AddAsync(AuditBase audit);
}