namespace Core.Interfaces;

public interface IIdMarker
{
	public string Id { get; set; }
}